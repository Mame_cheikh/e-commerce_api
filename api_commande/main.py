
import datetime
from flask import Flask, request, Response, jsonify
from flask_sqlalchemy import SQLAlchemy

#Pour gérer les mdp de manière hachée
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://test:test@db/test'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.secret_key = 'jknjknc6v468v86v354'


db = SQLAlchemy(app)

class Code_promos(db.Model):
    #Donnons un nom à cette table
    __tablename__ = "code_promos"

    id_code = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(100), index=True)
    pourcentage = db.Column(db.Integer, index=True)
    description = db.Column(db.String(200), index=True, unique=True)

    def __repr__(self):
        return '<Employées: {}>'.format(self.username)


class Commandes(db.Model):
    #Donnons un nom à cette table
    __tablename__ = "commandes"

    id_commande = db.Column(db.Integer, primary_key=True)
    id_utilisateur = db.Column(db.Integer, db.ForeignKey('utilisateurs.id_utilisateur'), nullable=False)
    status = db.Column(db.String(100), index=True)
    prix_in = db.Column(db.Integer, index=True)
    prix_fin = db.Column(db.Integer, index=True)
    date_commande = db.Column(db.Date)
    id_code = db.Column(db.Integer, db.ForeignKey('code_promos.id_code'),nullable=True)

    def __repr__(self):
        return '<Commandes: {}>'.format(self.id_commande)
    

class Utilisateurs(db.Model):
    #Donnons un nom à cette table
    __tablename__ = "utilisateurs"

    id_utilisateur = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(100), nullable=False)
    prenom = db.Column(db.String(100), index=True, nullable=False)
    email = db.Column(db.String(100), index=True, unique=True, nullable=False)
    password_hash = db.Column(db.String(128),nullable=True, index=True)
    is_invite = db.Column(db.Boolean)
    is_admin = db.Column(db.Boolean)
    is_vendeur = db.Column(db.Boolean, default=False)
    username = db.Column(db.String(60), index=True, unique=True)

    @property
    def password(self):
        """
        Prevent pasword from being accessed
        """
        raise AttributeError('password is not a readable attribute.')

    @password.setter
    def password(self, password):
        """
        Set password to a hashed password
        """
        self.password_hash = generate_password_hash(password_hash)

    def verify_password(self, password):
        """
        Check if hashed password matches actual password
        """
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<utilisateur: {}>'.format(self.username)

@app.route('/')
def hello_world():
    return '<h1> Hello AAD </h1>'

@app.route('/test')
def hello_AAD():
    return '<h1> Hello AAD, welcome to test </h1>'

@app.route('/hello')
def bdd():
    return Commandes.query.all()

@app.route('/allcommandes')
def get_all_commandes():
    #Recueillons les commandes de notre BDD
    commandes = Commandes.query.all()

    table_commandes=[]
    for commande in commandes:
        json_com={
            "id_commande": commande.id_commande,
            "id_utilisateur": commande.id_utilisateur,
            "status": commande.status,
            "prix_in": commande.prix_in,
            "prix_fin": commande.prix_fin,
            "id_code": commande.id_code,
            "date_commande": commande.date_commande
        }
        table_commandes.append(json_com)

    response = jsonify({"commandes":table_commandes})
    return response.json


"""
    Créons une requête pour recupérer une commande précise
"""    
@app.route('/commande/<int:id>', methods=['GET'])
def get_one_commande(id):
    #commande = Commandes.query.request.args.get('id')
    commande = Commandes.query.get(id)

    #Enregistrons la donnée sous forme JSON
    json_com={
        "id_commande": commande.id_commande,
        "id_utilisateur": commande.id_utilisateur,
        "status": commande.status,
        "prix_in": commande.prix_in,
        "prix_fin": commande.prix_fin,
        "id_code": commande.id_code,
        "date_commande": commande.date_commande
    }

    #Renvoyons la réponse sous forme JSON
    response = jsonify({"commandes":json_com})

    return response.json 


"""
    Créons une requête pour recupérer toutes les commandes en cours
"""    
@app.route('/commandes/encours', methods=['GET'])
def get_commandes_en_cours():
    #commande = Commandes.query.request.args.get('id')
    commandes = db.session.query(Commandes).filter(Commandes.status == 'En cours').all()
    
    #Enregistrons la donnée sous forme JSON
    table_commandes=[]
    for commande in commandes:
        json_com={
            "id_commande": commande.id_commande,
            "id_utilisateur": commande.id_utilisateur,
            "status": commande.status,
            "prix_in": commande.prix_in,
            "prix_fin": commande.prix_fin,
            "id_code": commande.id_code,
            "date_commande": commande.date_commande
        }
        table_commandes.append(json_com)
    
    #Renvoyons la réponse sous forme JSON
    response = jsonify({"commandes":table_commandes})
    return response.json

"""
    Créons une requête pour recupérer toutes les commandes livrée
"""    
@app.route('/commandes/livre', methods=['GET'])
def get_commandes_livre():
    commandes = db.session.query(Commandes).filter(Commandes.status == 'Livrée').all()
    
    #Enregistrons la donnée sous forme JSON
    table_commandes=[]
    for commande in commandes:
        json_com={
            "id_commande": commande.id_commande,
            "id_utilisateur": commande.id_utilisateur,
            "status": commande.status,
            "prix_in": commande.prix_in,
            "prix_fin": commande.prix_fin,
            "id_code": commande.id_code,
            "date_commande": commande.date_commande
        }
        table_commandes.append(json_com)
    
    #Renvoyons la réponse sous forme JSON
    response = jsonify({"commandes":table_commandes})
    return response.json

"""
    Créons une requête pour recupérer toutes les commandes d'un utilisateur
"""    
@app.route('/commandes/user/<int:id>', methods=['GET'])
def get_commandes_user(id):
    commandes = db.session.query(Commandes).filter(Commandes.id_utilisateur == id).all()
    
    #Enregistrons la donnée sous forme JSON
    table_commandes=[]
    for commande in commandes:
        json_com={
            "id_commande": commande.id_commande,
            "id_utilisateur": commande.id_utilisateur,
            "status": commande.status,
            "prix_in": commande.prix_in,
            "prix_fin": commande.prix_fin,
            "id_code": commande.id_code,
            "date_commande": commande.date_commande
        }
        table_commandes.append(json_com)
    
    #Renvoyons la réponse sous forme JSON
    response = jsonify({"commandes":table_commandes})
    return response.json

"""
    Créons une requête pour ajouter une commande 
"""    
@app.route('/addcommande', methods=['POST'])
def add_commande():
    commande = request.get_json()
    id = commande['id_commande']
    id_utilisateur = commande['id_utilisateur']
    status = commande['status']
    prix_in=commande['prix_in']
    prix_fin=commande['prix_fin']
    id_code=commande['id_code']

    new_commande = Commandes(id_commande=id,id_utilisateur=id_utilisateur,status=status,prix_in=prix_in,prix_fin=prix_fin,id_code=id_code, date_commande=datetime.datetime.now(datetime.timezone.utc))
    db.session.add(new_commande)
    db.session.commit()

    return 'Succès'

"""
    Créons une requête pour modifier le statut d'une commande 
"""    
@app.route('/editcommande/<int:id>', methods=['POST'])
def edit_commande(id):

    #Récupérons la commande en question
    commande = db.session.query(Commandes).filter(Commandes.id_commande == id).first()

    commande.status = request.get_json()['status']
    

    #Enregistrons dans la BDD
    db.session.add(commande)
    db.session.commit()

    return 'Succès'


"""
    Créons une requête pour supprimer une commande selon son numéro
"""
@app.route('/deletecommande/<int:id>', methods=['DELETE'])
def delete_commande(id):
    
    commande = db.session.query(Commandes).filter(Commandes.id_commande == id).first()

    db.session.delete(commande)
    db.session.commit()

    return 'Succès'


@app.route('/addcode', methods=['POST'])
def add_code():
    commande = request.get_json()
    id_code = commande['id_code']
    code = commande['code']
    pourcentage = commande['pourcentage']
    description=commande['description']
    

    new_code = Code_promos(id_code=id_code,code=code,pourcentage=pourcentage,description=description)
    db.session.add(new_code)
    db.session.commit()

    return 'Succès'

@app.route('/adduser', methods=['POST'])
def add_user():
    user = request.get_json()
    id_utilisateur = user['id_utilisateur']
    nom = user['nom']
    prenom = user['prenom']
    email=user['email']
    password=user['password']
    is_invite=user['is_invite']
    is_admin=user['is_admin']
    username = user['username']
    

    new_user = Utilisateurs(id_utilisateur=id_utilisateur,nom=nom,prenom=prenom,email=email, is_admin=is_admin,is_invite=is_invite, password_hash=password, username=username)
    db.session.add(new_user)
    db.session.commit()

    return 'Succès'



if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')