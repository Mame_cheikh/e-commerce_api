from flask import Flask, render_template, jsonify, Response, request
from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin
from flask_migrate import Migrate
from flask_login import LoginManager

from werkzeug.security import generate_password_hash, check_password_hash


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://test:test@db/test'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.secret_key = 'kdlsmjfioefsqlk'

db = SQLAlchemy(app)



class Utilisateurs(UserMixin, db.Model):
    """
    Create an user table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'utilisateurs'

    id_utilisateur = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(100), nullable=False)
    prenom = db.Column(db.String(100), index=True, nullable=False)
    email = db.Column(db.String(100), index=True, unique=True, nullable=False)
    password_hash = db.Column(db.String(128),nullable=True, index=True)
    is_invite = db.Column(db.Boolean, default=False)
    is_admin = db.Column(db.Boolean, default=False)
    is_vendeur = db.Column(db.Boolean, default=False)
    username = db.Column(db.String(60), index=True, unique=True)

    @property
    def password(self):
        """
        Prevent pasword from being accessed
        """
        raise AttributeError('password is not a readable attribute.')

    @password.setter
    def password(self, password):
        """
        Set password to a hashed password
        """
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """
        Check if hashed password matches actual password
        """
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<utilisateur: {}>'.format(self.username)





@app.route('/api/register/', methods = ['POST'])
def register():

    user = request.get_json()
    id_utilisateur = user['id_utilisateur']
    email = user['email']
    username = user['username']
    nom = user['nom']
    prenom = user['prenom']
    password = user['password']
    

    new_user = Utilisateurs(id_utilisateur = id_utilisateur ,email=email,username=username,nom=nom,prenom=prenom,password=password)


    db.session.add(new_user)
    db.session.commit()

    return 'Succés'

@app.route('/api/login/', methods = ['POST'])
def get_login():

    get_user = request.get_json()
    User = Utilisateurs.query.filter_by(email=get_user['email']).first()
    if User is not None:
        if User.verify_password(get_user['password']):


            json_log={

                    "id_utilisateur": User.id_utilisateur,
                    "email": User.email,
                    "username": User.username,
                    "nom": User.nom,
                    "prenom": User.prenom,
                    "password": get_user['password']
                    }

            response = jsonify(json_log)
            return response.json
        
    else:

        return 'erreur'

@app.route('/api/allusers/')
def get_all_users():
    users = Utilisateurs.query.all()

    table_users=[]
    for user in users:
        json_us={

            
           "email" : user.email,
            "username" : user.username,
            "nom": user.nom,
            "prenom" : user.prenom


        }
        table_users.append(json_us)
    response = jsonify({"users":table_users})
    return response.json

@app.route('/api/oneuser/<int:id_utilisateur>', methods = ['GET'])
def get_one_user(id_utilisateur):

    user = Utilisateurs.query.get(int(id_utilisateur)) 

    json_user={
            "id_utilisateur": user.id_utilisateur,
            "email": user.email,
            "username": user.username,
            "nom": user.nom,
            "prenom": user.prenom
            
    }

    response = jsonify({"utilisateurs":json_user})
    return response.json


@app.route('/api/oneuseremail/<string:email>', methods = ['GET'])
def get_one_user_by_mail(email):

    user = Utilisateurs.query.filter_by(email=email).first()
    response=jsonify({"rien":"rien"})
    if user : 
    
        json_user={

                "email": user.email,
                "username": user.username,
                "nom": user.nom,
                "prenom": user.prenom
                
        }

        response = jsonify({"utilisateurs":json_user})
    return response.json








@app.route('/api/usermax', methods = ['GET'])
def get_user_max():
    
    response = jsonify({'idmax': 0})
    if Utilisateurs.query.order_by(Utilisateurs.id_utilisateur.desc()).first():

               
        user = Utilisateurs.query.order_by(Utilisateurs.id_utilisateur.desc()).first()
        id_ut=user.id_utilisateur
        response = jsonify({'idmax': id_ut})
    return response.json

    
    

    
    
    





    


if __name__ == '__main__':
    #db.create_all()
    app.run(debug=True, host='0.0.0.0')

