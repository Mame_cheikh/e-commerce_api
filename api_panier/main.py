from flask import Flask, request, Response, jsonify

from flask import  redirect, request, url_for, jsonify
from flask_sqlalchemy import SQLAlchemy

#Pour gérer les mdp de manière hachée
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://test:test@db/test'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.secret_key = 'jknjknc6v468v86v354'


db = SQLAlchemy(app)

# création table Categorie
class Categorie(db.Model):
    """
    Create an Article table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'categories'

    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(60))
    sexe = db.Column(db.String(60))
    nomsexe = db.Column(db.Text)
    articles = db.relationship('Article', backref='categorie',
                                lazy='dynamic')


    def __repr__(self):
        return '<Categorie: {}>'.format(self.id)

#création table code promos
class Code_promos(db.Model):
    #Donnons un nom à cette table
    __tablename__ = "code_promos"

    id_code = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(100), index=True)
    pourcentage = db.Column(db.Integer, index=True)
    description = db.Column(db.String(200), index=True, unique=True)

    def __repr__(self):
        return '<Employées: {}>'.format(self.username)

#création table commandes
class Commandes(db.Model):
    #Donnons un nom à cette table
    __tablename__ = "commandes"

    id_commande = db.Column(db.Integer, primary_key=True)
    id_utilisateur = db.Column(db.Integer, db.ForeignKey('utilisateurs.id_utilisateur'), nullable=False)
    status = db.Column(db.String(100), index=True)
    prix_in = db.Column(db.Integer, index=True)
    prix_fin = db.Column(db.Integer, index=True)
    date_commande = db.Column(db.Date)
    id_code = db.Column(db.Integer, db.ForeignKey('code_promos.id_code'),nullable=True)

    def __repr__(self):
        return '<Commandes: {}>'.format(self.id_commande)
    
#création table Utilisateur
class Utilisateurs(db.Model):
    #Donnons un nom à cette table
    __tablename__ = "utilisateurs"

    id_utilisateur = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(100), nullable=False)
    prenom = db.Column(db.String(100), index=True, nullable=False)
    email = db.Column(db.String(100), index=True, unique=True, nullable=False)
    password_hash = db.Column(db.String(128))
    is_invite = db.Column(db.Boolean)
    is_admin = db.Column(db.Boolean)
    is_vendeur = db.Column(db.Boolean, default=False)
    username = db.Column(db.String(60), index=True, unique=True)

    @property
    def password(self):
        """
        Prevent pasword from being accessed
        """
        raise AttributeError('password is not a readable attribute.')

    @password.setter
    def password(self, password):
        """
        Set password to a hashed password
        """
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """
        Check if hashed password matches actual password
        """
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<utilisateur: {}>'.format(self.username)

# création table L_com_stock
class l_com_stock(db.Model):
    
    __tablename__ = 'L_com_stock'
    
    id= db.Column(db.Integer, primary_key=True)
    id_commande= db.Column(db.Integer)
    id_stock= db.Column(db.Integer)
    quantite=db.Column(db.Integer)

    def __repr__(self):
        return '<L_com_stock: {}>'.format(self.id)

#création table Photo
class Photo(db.Model):
    """
    Create an Article table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'photos'

    id = db.Column(db.Integer, primary_key=True)
    id_stock = db.Column(db.Integer, db.ForeignKey('stocks.id'))
    lien_photo = db.Column(db.String(60))
    position = db.Column(db.Integer)


    def __repr__(self):
        return '<Photo: {}>'.format(self.id)

#création table Avis
class Avis(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_article = db.Column(db.Integer, db.ForeignKey('articles.id'))
    note = db.Column(db.Integer)
    commentaire = db.Column(db.Text)
    id_utilisateur = db.Column(db.Integer)

    def __repr__(self):
        return '<Avis : {}'.format(self.id)



"""class stock(db.Model):
    
    __tablename__ = 'Stock'

    id_stock=db.Column(db.Integer, primary_key=True)
    id_article =db.Column(db.String(60))
    taille=db.Column(db.String(60))
    couleur=db.Column(db.String(60))
    prix_int=db.Column(db.Float)
    prix_fin=db.Column(db.Float)
    quantite =db.Column(db.Integer)
    solde=db.Column(db.Integer)
    def __repr__(self):
        return '<stock: {}>'.format(self.taille)"""

#création table Stock
class Stock(db.Model):
    """
    Create an Article table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'stocks'

    id = db.Column(db.Integer, primary_key=True)
    id_article = db.Column(db.Integer,db.ForeignKey('articles.id'))
    taille = db.Column(db.String(60))
    nom_couleur = db.Column(db.String(60))
    couleur = db.Column(db.String(60))
    prix_in = db.Column(db.Float)
    prix_fin = db.Column(db.Float)
    solde = db.Column(db.Integer)
    quantite = db.Column(db.Integer)
    photos = db.relationship('Photo', backref='photo',
                                lazy='dynamic')

    def __repr__(self):
        return '<Stock: {}>'.format(self.id)

"""class article(db.Model):

    __tablename__ = 'Article'
    id_article=db.Column(db.Integer, primary_key=True)
    nom =db.Column(db.String(60))
    id_categorie=db.Column(db.Integer)
    date_publication=db.Column(db.DateTime)



    def __repr__(self):
        return '<Article: {}>'.format(self.nom)"""

#Article
class Article(db.Model):
    """
    Create an Article table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'articles'

    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(60), index=True)
    description = db.Column(db.Text)
    photo = db.Column(db.Text)
    categorie_id = db.Column(db.Integer,db.ForeignKey('categories.id'))
    date_publication = db.Column(db.Date)
    stocks = db.relationship('Stock', backref='article',
                                lazy='dynamic')


    def __repr__(self):
        return '<Article: {}>'.format(self.nom)

"""
    Créons une requête pour modifier le statut d'une commande 
"""    

# méthode permettant de rajouter un code à une commande
@app.route('/addcode', methods=['POST'])
def add_code():

    commande = request.get_json()
    id_code = commande['id_code']
    code = commande['code']
    pourcentage = commande['pourcentage']
    description=commande['description']

    new_code = Code_promos(id_code=id_code,code=code,pourcentage=pourcentage,description=description)
    db.session.add(new_code)
    db.session.commit()

    return 'Succès'



# affiche tout les éléments du panier au format json
@app.route('/commande/article/<int:id>', methods=['GET'])
def lister(id):
    #commande = Commandes.query.request.args.get('id')
    c = Commandes.query.get(id)
    table=[]
    articles=l_com_stock.query.filter_by(id_commande=c.id_commande).all()
    for a in articles:
        nom=Article.query.filter_by(id=Stock.query.filter_by(id=a.id_stock).first().id_article).first().nom
        couleur=Stock.query.filter_by(id=a.id_stock).first().couleur
        idStock=Stock.query.filter_by(id=a.id_stock).first().id
        taille=Stock.query.filter_by(id=a.id_stock).first().taille
        prix=Stock.query.filter_by(id=a.id_stock).first().prix_in
        quantite=l_com_stock.query.filter_by(id=a.id_stock).first().quantite
        json_com={
        "nom": nom,
        "couleur": couleur,
        "idStock": idStock,
        "prix": prix,
        "taille": taille,
        "quantite": quantite,
        }
        table.append(json_com)
    #Renvoyons la réponse sous forme JSON
    response = jsonify({"articles":table})

    return response.json 
# Permet d'ajouter un article à la BDD
@app.route('/addarticle', methods=['POST'])
def add_article():
    commande = request.get_json()
    id_article = commande['id']
    nom = commande['nom']
    categorie_id = commande['id_categorie']
    date_publication=commande['date_publication']

    new_article = Article(id=id_article,nom=nom,categorie_id=categorie_id,date_publication=date_publication)
    db.session.add(new_article)
    db.session.commit()

    return 'Succès'
#permet d'ajouter un stock dans la BDD
@app.route('/addstock', methods=['POST'])
def add_stock():
    s = request.get_json()
    id=s['id']
    id_article =s['id_article']
    taille=s['taille']
    couleur=s['couleur']
    prix_in=s['prix_in']
    prix_fin=s['prix_fin']
    quantite =s['quantite']
    solde=s['solde']

    new_stock = Stock(id=id,id_article=id_article,taille=taille,couleur=couleur,prix_in=prix_in,prix_fin=prix_fin,quantite=quantite,solde=solde)
    db.session.add(new_stock)
    db.session.commit()

    return 'Succès'
# Permet d'ajouter un article au panier
@app.route('/addcom_stock/<int:id_c>/<int:id_s>', methods=['POST'])
def add_commande_stock(id_c,id_s):
    s = request.get_json()
    id=s['id']
    id_commande= id_c
    id_stock= id_s
    quantite=s['quantite']

    new_stock = l_com_stock(id=id,id_commande=id_commande,id_stock=id_stock,quantite=quantite)
    db.session.add(new_stock)
    db.session.commit()

    return 'Succès'

#############################################
# Methodes de AAD ;) #
##########################################""
@app.route('/del_com_stock/<int:id>', methods=['DELETE'])
def delete_commande_stock(id):
    c = l_com_stock.query.filter_by(id=id).first()
    db.session.delete(c)
    db.session.commit()

    return 'Succès'

@app.route('/show_com_stock', methods=['GET'])
def show_commande_stock():
    liste = l_com_stock.query.all()
    table = []
    for l in liste:
        json = {
            "id": l.id,
            "id_commande": l.id_commande,
            "id_stock": l.id_stock,
            "quantite": l.quantite
        }
        table.append(json)

    return jsonify({ "com_stock": table})



@app.route('/ajout_panier/<int:id_s>/<int:id_ut>', methods=['POST'])
def add_commande_stock_panier(id_s,id_ut):

    panier = Commandes.query.filter_by(status='traitement',id_utilisateur=id_ut).first()

    if panier is None:
        nbre_commandes=len(Commandes.query.all())
        c=Commandes(id_commande=nbre_commandes,id_utilisateur=id_ut,status='traitement',prix_in=0,prix_fin=0,id_code=0)
        db.session.add(c)
    else: 
        nbre_commandes=panier.id_commande

    s = request.get_json()
    #id=s['id']
    id_commande= nbre_commandes
    id_stock= id_s
    quantite=s['quantite']

    new_stock = l_com_stock(id_commande=id_commande,id_stock=id_stock,quantite=quantite)
    db.session.add(new_stock)
    db.session.commit()

    return 'Succès'
#Permet de créer une nouvel commande
@app.route('/addcommande', methods=['POST'])
def add_commande():
    s = request.get_json()
    id_commande= s['id_commande']
    id_utilisateur=s['id_utilisateur']
    status=s['status']
    prix_in=s['prix_in']
    prix_fin=s['prix_fin']
    id_code=s['id_code']

    new_commande = Commandes(id_commande=id_commande,id_utilisateur=id_utilisateur,status=status,prix_in=prix_in,prix_fin=prix_fin,id_code=id_code, date_commande = datetime.datetime.now(datetime.timezone.utc))
    db.session.add(new_commande)
    db.session.commit()

    return 'Succès'



#Permet d'afficher les informations propre au panier de l'utilisateur en cours
@app.route('/panier/<int:id>')
def liste(id):
    
    data=[]
    code=[]
    d=[]

    
    c=None
    c=Commandes.query.filter_by(status="traitement").filter_by(id_utilisateur=id).first()
    dictionnaire={'donne':'aucune'}
    cmd=[]
    if c!=None:
        
        cmd=[Commandes.query.filter_by(status="traitement").filter_by(id_utilisateur=id).first().id_commande]
        articles=l_com_stock.query.filter_by(id_commande=c.id_commande).all()
        for a in articles:
            nom=Article.query.filter_by(id=Stock.query.filter_by(id=a.id_stock).first().id_article).first().nom
            couleur=Stock.query.filter_by(id=a.id_stock).first().couleur
            idStock=Stock.query.filter_by(id=a.id_stock).first().id
            taille=Stock.query.filter_by(id=a.id_stock).first().taille
            prix=Stock.query.filter_by(id=a.id_stock).first().prix_in
            quantite=l_com_stock.query.filter_by(id=a.id).first().quantite
            data.append([nom,couleur,taille,prix,quantite, idStock])

        if Commandes.query.filter_by(id_commande=c.id_commande).first() :
            code=[Code_promos.query.filter_by(id_code=Commandes.query.filter_by(id_commande=c.id_commande).first().id_code).first().code,Code_promos.query.filter_by(id_code=Commandes.query.filter_by(id_commande=c.id_commande).first().id_code).first().description,Code_promos.query.filter_by(id_code=Commandes.query.filter_by(id_commande=c.id_commande).first().id_code).first().pourcentage]

    

   
        articles=l_com_stock.query.filter_by(id_commande=c.id_commande).all()
        for a in articles:
            d.append(Stock.query.filter_by(id=a.id_stock).first())
        total=0
        for a , b in zip(articles,d):
            total=total+(a.quantite * b.prix_fin)


        prixCommande=[Commandes.query.filter_by(id_commande=c.id_commande).first().prix_in,Commandes.query.filter_by(id_commande=c.id_commande).first().prix_fin]
        
        Commandes.query.filter_by(id_commande=c.id_commande).update({Commandes.prix_in: total })
        Commandes.query.filter_by(id_commande=c.id_commande).update({Commandes.prix_fin: total-total/100*code[2] })
        
        db.session.commit()
        dictionnaire={'panier':data,'code':code,'prixCommande': prixCommande,'commande':cmd}

    return jsonify(dictionnaire)


# Permet de supprimer un article d'un panier donné
@app.route('/panier/supprimer/<int:id>/<int:id_c>',methods=['GET', 'POST'])
def supprimer(id,id_c):
    
    c=   l_com_stock.query.filter_by(id_stock=id).first().id_commande
    l_com_stock.query.filter_by(id_stock=id).filter_by(id_commande=id_c).delete()
    db.session.commit()
    return redirect(url_for(".liste",id=Commandes.query.filter_by(id_commande=c).first().id_utilisateur))

#Permet d'annuler le bon de rédution appliquer sur une commande
@app.route('/coupon/<int:id>',methods=['GET', 'POST'])
def changer(id):

    Commandes.query.filter_by(id_commande=id).update({Commandes.id_code: 0 })
    Commandes.query.filter_by(id_commande=id).update({Commandes.prix_fin: Commandes.prix_in })
    db.session.commit()
    return redirect(url_for(".liste",id=Commandes.query.filter_by(id_commande=id).first().id_utilisateur))

#permet de supprimer toute les commandes de la BDD
@app.route('/deleteAll',methods=['GET', 'POST'])
def delete():
    c=Commandes.query.filter_by(status='valider').all()
    for a in c:
        db.session.delete(a)
    db.session.commit()

    return 'succes'





# Permet d'appliquer un bon de réduction sur une commande de lutilisateur
@app.route('/appliquer/<int:id>/<string:code>',methods=['GET', 'POST'])
def appliquer(id,code):
    
    if Code_promos.query.filter_by(code=code).first() :
        Commandes.query.filter_by(id_commande=id).update({Commandes.id_code:Code_promos.query.filter_by(code=code).first().id_code })
        prix=Commandes.query.filter_by(id_commande=id).first().prix_fin
        prix=prix-(prix*Code_promos.query.filter_by(code=code).first().pourcentage/100)
        Commandes.query.filter_by(id_commande=id).update({Commandes.prix_fin: prix })
        Commandes.query.filter_by(id_commande=id).update({Commandes.id_code: Code_promos.query.filter_by(code=code).first().id_code })
        db.session.commit()
    return redirect(url_for(".liste",id=Commandes.query.filter_by(id_commande=id).first().id_utilisateur))

# Permet la validation d'un panier celon son id
@app.route('/validation/<int:id>',methods=['GET', 'POST'])
def valider(id):
    
    c=Commandes(id_commande=len(Commandes.query.all()),id_utilisateur=Commandes.query.filter_by(id_commande=id).first().id_utilisateur,status='traitement',prix_in=0,prix_fin=0,id_code=0)
    db.session.add(c)
    Commandes.query.filter_by(id_commande=id).update({Commandes.status: 'En cours' })
    db.session.commit()

    return redirect(url_for(".liste",id=Commandes.query.filter_by(id_commande=id).first().id_utilisateur))









if __name__ == '__main__':
    db.create_all()
    app.run(debug=True, host='0.0.0.0')
